# Teste Automatizado de API usando Postam + Newman

Este projeto tem como finalizada contribuir para o aprendizado daqueles que desejam iniciar com testes de APIs de forma rápida e prática.

# api_serverest
 API usa para realizar os teste é a [ServeRest](https://serverest.dev/)

 O ServeRest é uma API REST gratuita que simula uma loja virtual com intuito de servir de material de estudos de testes de API.

Não deixe de seguir o autor do projeto e deixar um star no repositório: [Paulo Gonçalves](https://github.com/ServeRest/ServeRest)

## 🚀 Começando

Essas instruções permitirão que você obtenha uma cópia do projeto em operação na sua máquina local para fins de desenvolvimento e teste.

### 📋 Pré-requisitos

Baixa o projeto e para isso execute o comando: 

```
git clone https://gitlab.com/lindyQA/api_serverest.git
```

### 🔧 Instalação

O próximo passo é realizar as instalações das dependêcias:

```
Baixe e instale o NODE.js:https://nodejs.org/en
```

```
Baixe e instale o Postman:https://www.postman.com/downloads
```

```
Via linha de comando instale o Newman: npm install -g newman
```

```
Via linha de comando instale o HTMLExtra: npm install -g newman-reporter-htmlextra
```

## ⚙️ Executando os testes

Para executar os testes localmente execute por linha de comando: 

```
newman run teste_api_collection.json --folder "Usuario" -e env_api.json -r cli,htmlextra --reporter-htmlextra-title "Teste API - Usuário" --reporter-htmlextra-export public/report.html 
```

## 🛠️ Construído com

* [Javascript](https://www.javascript.com/) - Linguagem utilizada para desenvolver os testes
* [Postman](https://www.postman.com/) - Plataforma utilizada para criar os testes
* [Newman](https://www.npmjs.com/package/newman) - Executor de collections por linha de comando para o Postman

## ✒️ Autora

* **Paula Santiago** _QA_

## 🎁 Expressões de gratidão

* Conte a outras pessoas sobre este projeto 📢;
* Convide alguém da equipe para uma cerveja 🍺;
* Um agradecimento publicamente 🫂;
* Compartilhe uma pilula do seu conhecimento 📚;
* etc.

⌨️ com ❤️ por [Paula Santiago]() 😊